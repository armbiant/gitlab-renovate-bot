/*
 * Copyright (c) 2021 The Yaook Authors.
 *
 * This file is part of Yaook.
 * See https://yaook.cloud for further info.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
module.exports = {
    dryRun: process.env.CI_COMMIT_REF_SLUG == "master" ? null : "full",
    logLevel: "debug",
    platform: "gitlab",
    onboarding: false,
    requireConfig: "optional",
    printConfig: false,
    gitAuthor: "Renovate-Bot <renovate@yaook.cloud>",
    hostRules: [
      {
        hostType: 'docker',
        matchHost: 'process.env.CI_REGISTRY',
        username: 'process.env.CI_REGISTRY_USER',
        password: 'process.env.CI_REGISTRY_PASSWORD',
      },
    ],
    autodiscover: true,
    labels: ["Needs review"],
    enabledManagers: ["pip_setup", "dockerfile", "gitlabci", "pip_requirements", "regex"],
    assignees: [],
    assignAutomerge: false,
    'pip_requirements': {
      fileMatch: ["(^|/)([\\w-]*)requirements([\\w-]*)\\.(txt|pip)$"],
    },
    "allowedPostUpgradeCommands": ['^bash ci\/create_chore_release_note.sh$'],
};
